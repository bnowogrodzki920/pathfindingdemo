using System.Collections.Generic;
using UnityEngine;

namespace World.AStar
{
    public class AStarNode : MonoBehaviour
    {
        [SerializeField]
        private AStarController AStarController;

        [SerializeField]
        private Material _TraversableMaterial;
        
        [SerializeField]
        private Material _NotTraversableMaterial;
        
        [SerializeField]
        private List<AStarNode> _NeighbourNodes = new();
        public IReadOnlyList<AStarNode> NeighbourNodes => _NeighbourNodes;

        public Vector3 Position => transform.position;

        public Vector2Int Coords { get; private set; }
        
        public float FScore => GScore + HScore;
        public float GScore { get; set; }
        public float HScore { get; set; }
        
        public AStarNode PreviousNode { get; set; }

        private bool _IsTraversable;
        public bool IsTraversable
        {
            get => _IsTraversable;
            set
            {
                _IsTraversable = value;

                _Renderer.sharedMaterial = IsTraversable ? _TraversableMaterial : _NotTraversableMaterial;
            }
        }

        private Renderer _Renderer;

        private void Awake()
        {
            _Renderer = GetComponent<Renderer>();
        }

        public void Initialize(AStarController aStarController, bool isTraversable, Vector2Int coords)
        {
            AStarController = aStarController;
            IsTraversable = isTraversable;
            Coords = coords;
            name = $"AStarNode {Position}";
        }

        public void AdjustScale(float distanceBetweenNodes, float nodesOffset)
        {
            var xScale = distanceBetweenNodes - nodesOffset;
            var zScale = distanceBetweenNodes - nodesOffset;

            transform.localScale = new Vector3(xScale, transform.localScale.y, zScale);
        }
        
        public void AssignNeighbours()
        {
            AddNeighbourNode(GetNeighbourNode(0, 1));  // North
            AddNeighbourNode(GetNeighbourNode(0, -1)); // South
        
            if (Coords.x > 0)
            {
                AddNeighbourNode(GetNeighbourNode(-1, 0)); // East
            }
            
            if (Coords.x < AStarController.GridSize.x - 1)
            {
                AddNeighbourNode(GetNeighbourNode(1, 0));  // West
            }
            return;

            AStarNode GetNeighbourNode(int xOffset, int yOffset)
            {
                var x = Coords.x + xOffset;
                var y = Coords.y + yOffset;
                
                var index = y * AStarController.GridSize.y + x;

                if (index >= 0 && index < AStarController.Nodes.Count)
                {
                    return AStarController.Nodes[index];
                }
            
                return null;
            }

            void AddNeighbourNode(AStarNode neighbourNode)
            {
                if (neighbourNode == null)
                {
                    return;
                }
                
                _NeighbourNodes.Add(neighbourNode);
            }
        }

        public void ChangeMaterial(Material mat)
        {
            if (mat == null)
            {
                _Renderer.sharedMaterial = IsTraversable ? _TraversableMaterial : _NotTraversableMaterial;
                return;
            }
            
            _Renderer.sharedMaterial = mat;
        }

        #if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (UnityEditor.Selection.activeObject != gameObject)
            {
                return;
            }
            
            Gizmos.color = IsTraversable ? new Color(0, 0.75f, 0) : new Color(0.75f, 0, 0);
            Gizmos.DrawSphere(Position, 0.25f);

            foreach (var neighbourNode in _NeighbourNodes)
            {
                Gizmos.color = neighbourNode.IsTraversable ? new Color(0, 0.75f, 0.5f) : new Color(0.75f, 0, 0);
                Gizmos.DrawSphere(neighbourNode.Position, 0.2f);
            }
        }
        #endif
    }
}