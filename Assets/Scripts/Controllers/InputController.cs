using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Controllers
{
    public class InputController : MonoBehaviour
    {
        [SerializeField]
        private PlayerInput PlayerInput;

        public event Action ChangeTraversableEvent; 
        public event Action ChangePointEvent;
        public event Action LookEvent;
        
        public Vector2 CurrentMousePosition => Mouse.current.position.ReadValue();
        public Vector2 DeltaMousePosition => _PreviousMousePosition - CurrentMousePosition;
        public Vector2 CameraMoveVector => _MoveAction?.ReadValue<Vector2>() ?? Vector2.zero;
        public float ZoomAxis => Mathf.Clamp(_ZoomAction?.ReadValue<float>() ?? 0, -1, 1);

        private InputAction _MoveAction;
        private InputAction _ZoomAction;

        private Vector2 _PreviousMousePosition;
        
        private void Awake()
        {
            _MoveAction = PlayerInput.actions.FindAction("Move");
            _ZoomAction = PlayerInput.actions.FindAction("Zoom");
            PlayerInput.actions.FindAction("Rotate").performed += OnLook;
            PlayerInput.actions.FindAction("ChangeTraversable").performed += OnChangeTraversable;
            PlayerInput.actions.FindAction("ChangePoint").performed += OnChangePoint;
            StartCoroutine(EndFrameUpdate());
        }

        private IEnumerator EndFrameUpdate()
        {
            var waitForEndOfFrame = new WaitForEndOfFrame();
            
            while (Application.isPlaying)
            {
                _PreviousMousePosition = CurrentMousePosition;
                yield return waitForEndOfFrame;
            }
        }

        private void OnLook(InputAction.CallbackContext context)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }   
            
            LookEvent?.Invoke();
        }

        private void OnChangeTraversable(InputAction.CallbackContext context)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            
            ChangeTraversableEvent?.Invoke();
        }
        
        private void OnChangePoint(InputAction.CallbackContext context)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            
            ChangePointEvent?.Invoke();
        }

        public Vector3 GetScreenPositionConvertedToPlane(Vector3 screenPos, Vector3 planePoint)
        {
            var ray = Camera.main!.ScreenPointToRay(screenPos);
            var plane = new Plane(Vector3.up, planePoint);

            return !plane.Raycast(ray, out var enter) ? Vector3.zero : ray.GetPoint(enter);
        }
    }
}