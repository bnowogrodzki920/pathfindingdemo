using System;
using System.Linq;
using System.Threading.Tasks;
using Controllers;
using UnityEngine;
using World.AStar;

namespace DefaultNamespace
{
    public class PathVisualizationController : MonoBehaviour
    {
        [SerializeField]
        private InputController InputController;

        [SerializeField]
        private AStarController AStarController;


        [SerializeField]
        private Material _StartPointMaterial;
        
        [SerializeField]
        private Material _EndPointMaterial;
        
        [SerializeField]
        private Material _PathMaterial;
        
        public AStarNode StartPoint { get; private set; }
        
        public AStarNode EndPoint { get; private set; }

        public Path CurrentPath { get; private set; }
        
        public event Action StartPointSelectedEvent;
        public event Action StartPointDeselectedEvent;
        public event Action EndPointSelectedEvent;
        public event Action EndPointDeselectedEvent;
        public event Action PathRecreatedEvent;
        
        
        private async void Awake()
        {
            InputController.ChangePointEvent += OnChangePointType;

            await Task.Yield(); // wait for one frame to ensure proper registration
            InputController.ChangeTraversableEvent += TryRecreatePath;
        }

        private void OnChangePointType()
        {
            var closestNode = AStarController.FindClosestNodeByMousePosition(out _);
            
            if (StartPoint == null && EndPoint != closestNode)
            {
                StartPoint = closestNode;
                StartPoint.ChangeMaterial(_StartPointMaterial);
                StartPointSelectedEvent?.Invoke();
            }
            else if(StartPoint == closestNode)
            {
                StartPoint.ChangeMaterial(null);
                StartPoint = null;
                StartPointDeselectedEvent?.Invoke();
                ChangePathMaterials(null);
                CurrentPath = null;
            }
            else if (EndPoint == null)
            {
                EndPoint = closestNode;
                EndPoint.ChangeMaterial(_EndPointMaterial);
                EndPointSelectedEvent?.Invoke();
            }
            else if (EndPoint == closestNode)
            {
                EndPoint.ChangeMaterial(null);
                EndPoint = null;
                EndPointDeselectedEvent?.Invoke();
                ChangePathMaterials(null);
                CurrentPath = null;
            }
            
            TryRecreatePath();
        }

        private void TryRecreatePath()
        {
            if (StartPoint == null || EndPoint == null)
            {
                if (CurrentPath != null)
                {
                    ChangePathMaterials(null);
                }
                
                return;
            }

            if (CurrentPath != null && CurrentPath.Nodes.All(node => node.IsTraversable))
            {
                return;
            }
            
            ChangePathMaterials(null);
            CurrentPath = AStarController.FindPath(StartPoint, EndPoint);
            ChangePathMaterials(_PathMaterial);
            PathRecreatedEvent?.Invoke();
        }

        private void ChangePathMaterials(Material mat)
        {
            if (CurrentPath == null)
            {
                return;
            }
            
            foreach (var node in CurrentPath.Nodes)
            {
                node.ChangeMaterial(mat);
            }
        }
        
        private void OnDestroy()
        {
            InputController.ChangePointEvent -= OnChangePointType;
            InputController.ChangeTraversableEvent -= TryRecreatePath;
        }
    }
}