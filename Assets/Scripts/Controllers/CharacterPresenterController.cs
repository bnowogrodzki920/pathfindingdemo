using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Serialization;
using World.AStar;

namespace Controllers
{
    public class CharacterPresenterController : MonoBehaviour
    {
        [SerializeField]
        private PathVisualizationController PathVisualizationController;

        [SerializeField]
        private AStarController AStarController;

        [SerializeField]
        private ParticleSystem _SmokeParticle;
        
        [SerializeField]
        private GameObject _Character;

        [SerializeField]
        private float _MovementSpeed = 3;

        [SerializeField]
        private float _MovementSmoothTime = 0.2f;

        [SerializeField]
        private float _DistanceTolerance = 0.1f;
        
        [SerializeField]
        private float _Maneuverability = 3;

        [SerializeField]
        private int _PathSplineResolution = 10;

        private int _SpeedAnimationID;
        private Animator _CharacterAnimator;
        
        private Coroutine _MovementRoutine;
        
        private void Awake()
        {
            _Character.SetActive(false);
            _CharacterAnimator = _Character.GetComponent<Animator>();
            _SpeedAnimationID = Animator.StringToHash("Speed");

            PathVisualizationController.StartPointSelectedEvent += SpawnCharacter;
            PathVisualizationController.StartPointDeselectedEvent += DespawnCharacter;
            PathVisualizationController.PathRecreatedEvent += GoToEndPoint;
            PathVisualizationController.EndPointDeselectedEvent += GoToStartPoint;
        }
        
        private void SpawnCharacter()
        {
            _Character.SetActive(true);
            MoveToStartPoint();
        }

        private void MoveToStartPoint(bool useParticles = true)
        {
            AStarController.Warp(_Character.transform, PathVisualizationController.StartPoint);
            _CharacterAnimator.SetFloat(_SpeedAnimationID, 0);

            if (useParticles)
            {
                PlaySmokeParticleAt(_Character.transform.position);
            }
        }
        
        private void DespawnCharacter()
        {
            _Character.SetActive(false);
            PlaySmokeParticleAt(_Character.transform.position);
            _MovementRoutine = null;
        }

        private void GoToEndPoint()
        {
            if (_MovementRoutine != null)
            {
                PlaySmokeParticleAt(_Character.transform.position);
                MoveToStartPoint();
                StopCoroutine(_MovementRoutine);
                _MovementRoutine = null;
            }

            if (transform.position != PathVisualizationController.StartPoint.Position)
            {
                MoveToStartPoint();
            }

            _MovementRoutine = StartCoroutine(GoToEndPointRoutine());
        }

        private IEnumerator GoToEndPointRoutine()
        {
            var points = PathVisualizationController.CurrentPath.GenerateSpline(_Character.transform.position, _PathSplineResolution);

            if (points.Count <= 2)
            {
                yield break;
            }

            var startingNodeIndex = 0;
            var endingNodeIndex = PathVisualizationController.CurrentPath.Nodes.Count - 1;
            
            if (PathVisualizationController.CurrentPath.Nodes.Count > 1)
            {
                var startingNodeSearchIndex = (_Character.transform.position - PathVisualizationController.CurrentPath.Nodes[0].Position).sqrMagnitude < _DistanceTolerance * _DistanceTolerance ? 1 : 0;
                startingNodeIndex = points.FindIndex(point => (PathVisualizationController.CurrentPath.Nodes[startingNodeSearchIndex].Position - point).sqrMagnitude < _DistanceTolerance * _DistanceTolerance);
                var endingNodeSearchIndex = ^Mathf.Clamp(2, 0, PathVisualizationController.CurrentPath.Nodes.Count - 1);
                endingNodeIndex = points.FindIndex(point => (PathVisualizationController.CurrentPath.Nodes[endingNodeSearchIndex].Position - point).sqrMagnitude < _DistanceTolerance * _DistanceTolerance);
            }

            var aggregatedStartingDistance = GetSqrDistance(startingNodeIndex);
            var aggregatedEndingDistance = GetSqrDistance(points.Count, endingNodeIndex);
            
            for (var i = 0; i < points.Count; i ++)
            {
                var nextPosition = points[i];
                
                while ((_Character.transform.position - nextPosition).sqrMagnitude > _DistanceTolerance * _DistanceTolerance)
                {
                    var currentMovementSpeed = _MovementSpeed;

                    if (i < startingNodeIndex)
                    {
                        var currentSqrDistance = GetSqrDistance(i);
                        currentSqrDistance += (_Character.transform.position - nextPosition).sqrMagnitude;
                        currentMovementSpeed = Extensions.EaseOutQuad(0.1f, _MovementSpeed, currentSqrDistance / aggregatedStartingDistance);
                    }
                    else if (i > endingNodeIndex)
                    {
                        var currentSqrDistance = GetSqrDistance(i, endingNodeIndex);
                        //currentSqrDistance += (Character.transform.position - nextPosition).sqrMagnitude;
                        currentMovementSpeed = Extensions.EaseInQuad(_MovementSpeed, 0.1f, currentSqrDistance / aggregatedEndingDistance);
                    }
                    
                    _Character.transform.position = Vector3.MoveTowards(_Character.transform.position, nextPosition, currentMovementSpeed * Time.deltaTime);
                    _CharacterAnimator.SetFloat(_SpeedAnimationID, currentMovementSpeed);
                    
                    RotateTowards(nextPosition);
                    yield return null;
                }
            }

            _CharacterAnimator.SetFloat(_SpeedAnimationID, 0);
            _MovementRoutine = null;

            yield break;

            float GetSqrDistance(int lastIndex, int startingIndex = 1)
            {
                if (lastIndex > points.Count)
                {
                    lastIndex = points.Count;
                }

                if (startingIndex < 1)
                {
                    startingIndex = 1;
                }

                var sqrDistance = 0f;
                
                for (var i = startingIndex; i < lastIndex; i++)
                {
                    sqrDistance += (points[i - 1] - points[i]).sqrMagnitude;
                }

                return sqrDistance;
            }
        }

        private void RotateTowards(Vector3 nextPosition)
        {
            var direction = (nextPosition - _Character.transform.position).normalized;
            var newDirection = Vector3.RotateTowards(_Character.transform.forward, direction, _Maneuverability * Time.deltaTime, 0f);
            _Character.transform.rotation = Quaternion.LookRotation(newDirection);
        }
        
        private void GoToStartPoint()
        {
            if (_MovementRoutine != null)
            {
                StopCoroutine(_MovementRoutine);
            }

            if (PathVisualizationController.StartPoint == null)
            {
                return;
            }
            
            MoveToStartPoint();
        }

        private void OnDestroy()
        {
            PathVisualizationController.StartPointSelectedEvent -= SpawnCharacter;
            PathVisualizationController.StartPointDeselectedEvent -= DespawnCharacter;
            PathVisualizationController.PathRecreatedEvent -= GoToEndPoint;
            PathVisualizationController.EndPointDeselectedEvent -= GoToStartPoint;
        }

        private void PlaySmokeParticleAt(Vector3 point)
        {
            if (!_SmokeParticle.gameObject.activeSelf)
            {
                _SmokeParticle.gameObject.SetActive(true);
            }
            
            _SmokeParticle.transform.position = point;
            _SmokeParticle.Play();
        }
    }
}