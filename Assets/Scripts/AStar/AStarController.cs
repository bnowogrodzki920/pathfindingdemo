using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Controllers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace World.AStar
{
    public class AStarController : MonoBehaviour
    {
        [SerializeField]
        private InputController InputController;
        
        [SerializeField]
        private List<AStarNode> _Nodes = new();

        public IReadOnlyList<AStarNode> Nodes => _Nodes;

        [SerializeField, Range(0, 1)]
        private float _NotTraversableNodeSpawnChance = 0.2f;

        [SerializeField]
        private Vector2Int _GridSize = new(30, 30);
        public Vector2Int GridSize => new(Mathf.Abs(_GridSize.x), Mathf.Abs(_GridSize.y));

        [SerializeField]
        private float _DistanceBetweenNodes = 1.5f;
        
        [SerializeField]
        private float _NodesOffset = 0.1f;

        [SerializeField]
        private AStarNode _NodeAsset;

        public event Action OnNodesGenerated;

        public void Awake()
        {
            RegenerateNodes();
            
            InputController.ChangeTraversableEvent += OnChangeTraversable;
        }

        private void OnChangeTraversable()
        {
            var closestNode = FindClosestNodeByMousePosition(out var mousePosOnPlane);
            var distance = (mousePosOnPlane - closestNode.Position).magnitude;

            if (distance < closestNode.transform.localScale.x + _NodesOffset ||
                distance < closestNode.transform.localScale.z + _NodesOffset)
            {
                closestNode.IsTraversable = !closestNode.IsTraversable;
            }
        }

        private async void RegenerateNodes()
        {
            float absGridSizeX = Mathf.Abs(GridSize.x);
            float absGridSizeY = Mathf.Abs(GridSize.y);
            
            for (var y = 0; y < absGridSizeY; y++)
            {
                for (var x = 0; x < absGridSizeX; x++)
                {
                    var isTraversable = Random.value > _NotTraversableNodeSpawnChance;
                    var xPos = (x * _DistanceBetweenNodes) - (absGridSizeX * _DistanceBetweenNodes / 2f);
                    var zPos = (y * _DistanceBetweenNodes) - (absGridSizeY * _DistanceBetweenNodes / 2f);
                    var nodePos = new Vector3(xPos, transform.position.y, zPos);

                    var node = Instantiate(_NodeAsset, nodePos, Quaternion.identity, transform);
                    node.Initialize(this, isTraversable, new Vector2Int(x, y));
                    node.AdjustScale(_DistanceBetweenNodes, _NodesOffset);
                    _Nodes.Add(node);
                }

                await Task.Yield();
            }

            foreach (var node in _Nodes)
            {
                node.AssignNeighbours();
            }

            OnNodesGenerated?.Invoke();
        }

        public Path FindPath(Vector3 from, Vector3 to)
        {
            var startNode = FindClosestNode(from);
            var endNode = FindClosestNode(to);
            return FindPath(startNode, endNode);
        }
        
        public Path FindPath(AStarNode startNode, AStarNode endNode)
        {
            var closedSet = new HashSet<AStarNode>();
            var openSet = new List<AStarNode>() { startNode };

            if (!endNode.IsTraversable)
            {
                Debug.Log($"[{nameof(AStarController)}.{nameof(FindPath)}] Found End Node is not walkable! Trying to find path as close as possible to this node", endNode);
                endNode = endNode.NeighbourNodes.FirstOrDefault(node => node.IsTraversable);

                if (endNode == null)
                {
                    Debug.Log($"[{nameof(AStarController)}.{nameof(FindPath)}] Couldn't find close walkable node to {endNode}! Aborting...", endNode);
                    return null;
                }
            }

            while (openSet.Count > 0)
            {
                var bestF = 0;

                for (var i = 0; i < openSet.Count; i++)
                {
                    if (openSet[i].FScore < openSet[bestF].FScore)
                    {
                        bestF = i;
                    }
                }

                var currentNode = openSet[bestF];

                if (currentNode == endNode)
                {
                    Debug.Log($"[{nameof(AStarController)}.{nameof(FindPath)}] Found path from {startNode.Position} to {endNode.Position}!", endNode);
                    return RecreatePath(currentNode, endNode.Position);
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                foreach (var neighbourNode in currentNode.NeighbourNodes)
                {
                    if (closedSet.Contains(neighbourNode) || !neighbourNode.IsTraversable)
                    {
                        continue;
                    }

                    var currentG = currentNode.GScore + GetHeuristicScore(currentNode, neighbourNode);
                    var foundNewPath = false;

                    if (openSet.Contains(neighbourNode))
                    {
                        if (currentG < neighbourNode.GScore)
                        {
                            neighbourNode.GScore = currentG;
                            foundNewPath = true;
                        }
                    }
                    else
                    {
                        neighbourNode.GScore = currentG;
                        foundNewPath = true;
                        openSet.Add(neighbourNode);
                    }

                    if (!foundNewPath)
                    {
                        continue;
                    }

                    neighbourNode.HScore = GetHeuristicScore(neighbourNode, endNode);
                    neighbourNode.PreviousNode = currentNode;
                }
            }

            Debug.LogError($"[{nameof(AStarController)}.{nameof(FindPath)}] Couldn't find path to {endNode.Position}!", endNode);
            return null;
        }

        private Path RecreatePath(AStarNode currentNode, Vector3 endPoint)
        {
            var pathNodes = new List<AStarNode>();

            while (currentNode.PreviousNode != null)
            {
                if (pathNodes.Contains(currentNode.PreviousNode))
                {
                    break;
                }

                pathNodes.Add(currentNode.PreviousNode);
                currentNode = currentNode.PreviousNode;
            }

            pathNodes.Reverse();
            return new Path(pathNodes.Skip(1).ToList(), endPoint); // Can skip first node because it's almost equal to current starting position
        }

        public AStarNode FindClosestNodeByMousePosition(out Vector3 mousePosOnPlane)
        {
            mousePosOnPlane = InputController.GetScreenPositionConvertedToPlane(InputController.CurrentMousePosition, transform.position);
            return FindClosestNode(mousePosOnPlane);
        }

        private AStarNode FindClosestNode(Vector3 position)
        {
            var closestNode = _Nodes[0];
            var minSqrDist = float.MaxValue;

            foreach (var node in _Nodes)
            {
                var sqrDist = (node.Position - position).sqrMagnitude;

                if (sqrDist > minSqrDist)
                {
                    continue;
                }

                minSqrDist = sqrDist;
                closestNode = node;
            }

            return closestNode;
        }

        private float GetHeuristicScore(AStarNode aNode, AStarNode bNode)
        {
            return Vector3.Distance(aNode.Position, bNode.Position);
        }

        private void OnDrawGizmosSelected()
        {
            foreach (var node in _Nodes)
            {
                Gizmos.color = node.IsTraversable ? new Color(0, 0.75f, 0) : new Color(0.75f, 0, 0);
                Gizmos.DrawSphere(node.Position, 0.2f);
            }
        }

        public void Warp(Transform t)
        {
            var node = FindClosestNode(t.position);
            Warp(t, node);
        }

        public void Warp(Transform t, AStarNode node)
        {
            var warpedPosition = node.Position;
            t.position = warpedPosition;
        }
    }
}