using System;
using System.Collections;
using UnityEngine;

namespace Controllers
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private Camera Camera;
        
        [SerializeField]
        private InputController InputController;

        [SerializeField]
        private Vector3 _MinWorldBounds;
        
        [SerializeField]
        private Vector3 _MaxWorldBounds;
        
        [SerializeField]
        private float _RotationSpeed = 0.5f;
        
        [SerializeField]
        private bool _InverseRotation;
        
        [SerializeField]
        private float _MoveSpeed = 1f;

        [SerializeField]
        private float _ZoomSpeed = 1f;
        
        [SerializeField, Range(0, 1)]
        private float _ZoomSmoothingTime = 0.2f;
        
        private bool _IsRotating;

        private float _CurrentVerticalVelocity;
        private float _CurrentZoomSpeed;
        
        private void Awake()
        {
            InputController.LookEvent += OnLook;
        }

        private void OnLook()
        {
            _IsRotating = !_IsRotating;
        }

        private void LateUpdate()
        {
            if (!_IsRotating)
            {
                Move();
                Zoom();
            }
            else
            {
                Rotate();
            }
        }

        private void Rotate()
        {
            var cameraMiddlePoint = InputController.GetScreenPositionConvertedToPlane(Camera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f)), InputController.transform.position);
            var axis = MathF.Sign(InputController.DeltaMousePosition.x);
            transform.RotateAround(cameraMiddlePoint, Vector3.up, _InverseRotation ? axis * _RotationSpeed * Time.deltaTime : -axis * _RotationSpeed);
            transform.position = ClampVector(transform.position, _MinWorldBounds, _MaxWorldBounds);
        }

        private void Move()
        {
            var moveVector = new Vector3
            {
                x = InputController.CameraMoveVector.x,
                y = 0,
                z = InputController.CameraMoveVector.y
            };
            var angle = Vector3.SignedAngle(Vector3.forward, transform.forward, Vector3.up);
            var rotatedVector = Quaternion.AngleAxis(angle, Vector3.up) * moveVector;
            var newPosition = Vector3.MoveTowards(transform.position, transform.position + rotatedVector, _MoveSpeed * Time.deltaTime);
            transform.position = ClampVector(newPosition, _MinWorldBounds, _MaxWorldBounds);
        }

        private void Zoom()
        {
            var newPosition = transform.position;
            _CurrentZoomSpeed = Mathf.SmoothDamp(_CurrentZoomSpeed, _ZoomSpeed * InputController.ZoomAxis, ref _CurrentVerticalVelocity, _ZoomSmoothingTime);
            newPosition.y += _CurrentZoomSpeed;
            transform.position = ClampVector(newPosition, _MinWorldBounds, _MaxWorldBounds);
        }

        private Vector3 ClampVector(Vector3 vector, Vector3 minVector, Vector3 maxVector)
        {
            return new Vector3(Mathf.Clamp(vector.x, minVector.x, maxVector.x),
                               Mathf.Clamp(vector.y, minVector.y, maxVector.y),
                               Mathf.Clamp(vector.z, minVector.z, maxVector.z));
        }

        private void OnDestroy()
        {
            InputController.LookEvent -= OnLook;
        }
    }
}